# Functional programming paradigm applied to package management:
## toward reproducible computational environment}

Talk for [IRILL](https://www.irill.org/) seminar.


# Build

```
guix time-machine -C channels.scm \
     -- shell -m manifest.scm     \
     -- rubber --pdf stournier-Guix-20230223.tex
```

+ Git: https://gitlab.com/zimoun/irill-2023
+ SWH: https://archive.softwareheritage.org/swh:1:dir:965749f690abbd3c33a9d27dae3abc7d1cf65677
