(define python
  (package
    (name "python")
    (version "3.9.9")
    (source ... )                   ;points to URL source code
    (build-system gnu-build-system) ;./configure & make
    (arguments ... )                ; configure flags, etc.
    (inputs (list bzip2 expat gdbm libffi sqlite
                  openssl readline zlib tcl tk))))
