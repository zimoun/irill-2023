#!/usr/bin/env bash

guix time-machine -C channels.scm                \
     -- shell --container -m manifest.scm        \
     -- rubber --pdf stournier-Guix-20230223.tex
